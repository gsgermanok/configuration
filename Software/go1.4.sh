#!/bin/bash -l

###########################################
# Go 1.4
###########################################

environment() {
  export GOARCH=amd64
  export GOPATH=/home/ggerman/go
  export GOBIN=/home/ggerman/go/go1.4/bin
  export GOROOT=/home/ggerman/go
  
  unset NDK_ROOT

  export PATH=$PATH:$GOPATH/go1.4/bin
  export GOTOOLDIR=/home/ggerman/go/go/pkg/tool/linux_amd64
  export CC="gcc"
  export GOGCCFLAGS="-fPIC -m64 -pthread -fmessage-length=0"
  export CXX=g++
  export CGO_ENABLED=1

  unset CC_FOR_TARGET
}

environment

